#!/bin/bash
### OUTSIDE CHROOT
mount --bind /sys /target/sys
mount --bind /proc /target/proc
mount --bind /dev /target/dev
chroot /target
### INSIDE CHROOT
dpkg-reconfigure -p low grub-efi-amd64
grub-install --force --target=x86_64-efi --no-nvram /dev/nvme1
grub-install --force --recheck --no-nvram /dev/nvme0
grub-install --force --target=x86_64-efi --no-nvram /dev/nvme0
grub-install --force --recheck --no-nvram /dev/nvme1
efibootmgr -c -D -L "Ubuntu (nvme0)" -d /dev/nvme0n1 -p1
efibootmgr -c -D -L "Ubuntu (nvme1)" -d /dev/nvme1n1 -p1

### IN GRUB
# linux /vmlinuz
# partition /dev/md127p6
# root=/dev/md127p6
#
# root=(md/0,5)
# linux=vmlinuz root=/dev/md0p5
# initrd /initrd
# boot

# NOTES:
# Maybe one should reassemble the md array while in chroot? And then update-grub?

# Challenging Ubuntu Installation

# Steps

* Boot up a live ubuntu
* Set locale for kbd (tbd)
* ctl-alt-t

```sh
sudo -i
apt update
apt install mdadm
mdadm --create --verbose /dev/md127 --level=1 --metadata=1.0
parted /dev/md127 mklabel gpt
parted mkpart efi fat32 1024kiB 550MB
parted mkpart root ext4 551MB 100%
mkdir -p /mnt/target/boot/efi
mkfs.fat /dev/md127p1
mkfs.ext4 /dev/md127p2
mount /dev/md127p1 /mnt/target/boot/efi
mount /dev/md127p2 /mnt/target
apt install debootstrap
debootstrap --arch amd64 bionic /mnt/target
LANG=C.UTF-8 chroot /mnt/target /bin/bash
export TERM=xterm-color
cat <<EOF >/etc/apt/sources.list
deb http://fi.archive.ubuntu.com/ubuntu/ bionic main universe
deb http://fi.archive.ubuntu.com/ubuntu/ bionic-updates universe
deb http://security.ubuntu.com/ubuntu bionic-security main universe
EOF

apt update
apt install makedev
mount none /proc -t proc
cd /dev
MAKEDEV std
exit
mount --bind /dev /mnt/target/dev
LANG=C.UTF-8 chroot /mnt/target /bin/bash
mount -t sysfs sysfs /sys
cat <<EOF >/etc/fstab
/dev/md127p1    /boot/efi       vfat    defaults        0       1
/dev/md127p2    /               ext4    defaults        0       1
/swapfile       none            swap    sw              0       0
EOF

dpkg-reconfigure tzdata
cat <<EOF >/etc/netplan/01-netcfg.yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    enp3s0:
      dhcp4: true
EOF

cat <<EOF >/etc/hosts
127.0.0.1 localhost
EOF

apt install -y language-pack-fi console-setup
# Generic 105 (intl)
# Finnish
# Finnish
# Default
# No compose
dpkg-reconfigure keyboard-configuration
#apt install -y linux-image-4.15.0-74-generic
apt install -y linux-image-generic-18.04
apt install -y grub-efi-amd64-signed
#apt install mdadm
grub-install --target=x86_64-efi --no-nvram /dev/nvme1n1
grub-install --recheck --no-nvram /dev/nvme0n1
grub-install --target=x86_64-efi --no-nvram /dev/nvme0n1
grub-install --recheck --no-nvram /dev/nvme1n1
efibootmgr -c -D -L "Ubuntu (NVMe0)" -d /dev/nvme0n1 -p1
efibootmgr -c -D -L "Ubuntu (NVMe1)" -d /dev/nvme1n1 -p1
update-grub
# Create users!
MAKE THEM FUCKING ADMINS
```

